-- Table creation

DROP TABLE Equipment;
DROP TABLE Employee;
DROP TABLE Team;
DROP TABLE Department;

CREATE TABLE Department (
  id          NUMBER(5)     NOT NULL PRIMARY KEY,
  name        VARCHAR2(256) NOT NULL,
  description VARCHAR2(512) NOT NULL,
  floors      NUMBER(5)     NOT NULL
);

CREATE TABLE Team (
  id             NUMBER(5)     NOT NULL PRIMARY KEY,
  name           VARCHAR2(256) NOT NULL,
  teamSize       NUMBER(5)     NOT NULL,
  fkDepartmentId NUMBER(5)     NOT NULL,
  FOREIGN KEY (fkDepartmentId) REFERENCES Department (ID)
)

PCTFREE 10 PCTUSED 60 STORAGE (INITIAL 7K NEXT 7K PCTINCREASE 0 MAXEXTENTS 10
);


CREATE TABLE Employee (
  id             NUMBER(5)     NOT NULL PRIMARY KEY,
  fullName       VARCHAR2(256) NOT NULL,
  job            VARCHAR2(256) NOT NULL,
  additionalInfo VARCHAR2(256),
  salary         NUMBER(10)    NOT NULL,
  fkTeamId       NUMBER(5)     NOT NULL,
  FOREIGN KEY (fkTeamId) REFERENCES Team (ID)
);

CREATE TABLE Equipment (
  id          NUMBER(5)     NOT NULL PRIMARY KEY,
  name        VARCHAR2(128) NOT NULL,
  description VARCHAR2(512) NOT NULL,
  quantity    NUMBER(10)    NOT NULL,
  fkOwnerId   NUMBER(5)     NOT NULL,
  fkTeamId    NUMBER(5)     NOT NULL,
  FOREIGN KEY (fkOwnerId) REFERENCES Employee (ID),
  FOREIGN KEY (fkTeamId) REFERENCES Team (ID)
);

-- 2.

INSERT INTO Department VALUES (0, 'First department', 'Nice department', 2);
INSERT INTO Department VALUES (1, 'Second department', 'Bad department', 4);

INSERT INTO Team VALUES (0, 'Red', 3, 0);
INSERT INTO Team VALUES (1, 'Blue', 1, 0);
INSERT INTO Team VALUES (2, 'Yellow', 2, 1);
INSERT INTO Team VALUES (3, 'Green', 2, 1);

INSERT INTO Employee VALUES (0, 'Willie E. Pompey', 'developer', '', 100, 0);
INSERT INTO Employee VALUES (1, 'Chad R. Williams', 'manager', '', 150, 0);
INSERT INTO Employee VALUES (2, 'Ira B. Moore', 'manager', '', 250, 0);
INSERT INTO Employee VALUES (3, 'Bryce P. Griffith', 'manager', '', 400, 1);
INSERT INTO Employee VALUES (4, 'Danial K. Ambrose', 'producer', '', 125, 2);
INSERT INTO Employee VALUES (5, 'Santos D. McKeon', 'developer', '', 110, 2);
INSERT INTO Employee VALUES (6, 'John B. Pannell', 'developer', '', 90, 3);
INSERT INTO Employee VALUES (7, 'Joseph T. Kime', 'developer', '', 120, 3);

INSERT INTO Equipment VALUES (0, 'Pen', 'Shinny pen', 5, 0, 0);
INSERT INTO Equipment VALUES (1, 'Monitor', '24 inch Monitor', 2, 1, 0);
INSERT INTO Equipment VALUES (2, 'Monitor', '20 inch Monitor', 1, 0, 0);
INSERT INTO Equipment VALUES (3, 'Pen', 'Shinny pen', 2, 3, 1);
INSERT INTO Equipment VALUES (4, 'Keyboard', 'Mechanical keyboard', 1, 5, 2);
INSERT INTO Equipment VALUES (5, 'Keyboard', 'Mechanical keyboard', 1, 6, 3);
INSERT INTO Equipment VALUES (6, 'Keyboard', 'Mechanical keyboard', 1, 7, 3);

SELECT
  TABLESPACE_NAME,
  PCT_FREE,
  PCT_USED,
  INITIAL_EXTENT,
  NEXT_EXTENT,
  MIN_EXTENTS,
  MAX_EXTENTS,
  PCT_INCREASE
FROM USER_TABLES
WHERE TABLE_NAME = 'EQUIPMENT';

--

CREATE INDEX EmployeeIndex
ON Employee (job, salary);

-- View creation

CREATE VIEW departments_equipment_sum(department, count) AS
  SELECT
    Department.name,
    SUM(Equipment.quantity)
  FROM Department, Team, Equipment
  WHERE Department.id = Team.fkDepartmentId AND Equipment.fkTeamId = Team.id
  GROUP BY Department.name;

SELECT *
FROM departments_equipment_sum;

-- Functions

-- set serveroutput on size 30000; -- in console to view output....

-- TODO function example to operate on strings

CREATE OR REPLACE FUNCTION get_department_teams_count(department_name IN Department.name%TYPE)
  RETURN NUMBER
AS teams_count NUMBER;
  BEGIN
    SELECT COUNT(*)
    INTO teams_count
    FROM Department, Team
    WHERE Department.name = department_name AND Team.fkDepartmentId = Department.id;
    RETURN (teams_count);
  END;

DECLARE
  res NUMBER(2);
BEGIN
  res := get_department_teams_count('Second department');
  dbms_output.put_line('Total no. of Teams: ' || res);
END;

CREATE OR REPLACE FUNCTION get_department_equipment_count(department_name IN Department.name%TYPE,
                                                          equipment_name  IN Equipment.name%TYPE)
  RETURN NUMBER
AS equipment_count NUMBER;
  BEGIN
    SELECT SUM(Equipment.quantity)
    INTO equipment_count
    FROM Department, Team, Equipment
    WHERE Department.name = department_name AND Team.fkDepartmentId = Department.id AND Equipment.fkTeamId = Team.id AND
          Equipment.name = equipment_name;
    RETURN (equipment_count);
  END;

DECLARE
  res NUMBER(2);
BEGIN
  res := get_department_equipment_count('Second department', 'Keyboard');
  dbms_output.put_line('Total no. of Equipment: ' || res);
END;


-- Lab2 - Cursors

CREATE OR REPLACE PROCEDURE update_job_title(job_name IN Employee.job%TYPE, medianSalary IN Employee.salary%TYPE)
AS
  BEGIN
    UPDATE Employee
    SET Employee.additionalInfo = CASE WHEN (Employee.salary < medianSalary)
      THEN concat(Employee.additionalInfo, 'Low income')
                                  ELSE concat(Employee.additionalInfo, 'High income') END
    WHERE Employee.job = job_name;
  END;

BEGIN
  update_job_title('developer', 110);
END;

CREATE TABLE list_of_dept_equipment (
  deptId   NUMBER(5)     NOT NULL PRIMARY KEY,
  deptName VARCHAR2(256) NOT NULL,
  list     VARCHAR2(2048)
);

CREATE OR REPLACE PROCEDURE populate_dept_equipment AS
  CURSOR list_dept IS
    SELECT
      Department.id,
      Department.name
    FROM Department;
  CURSOR list_equipment(dept_id IN Department.id%TYPE) IS
    SELECT
      Equipment.name,
      SUM(Equipment.quantity)
    FROM Equipment, Team
    WHERE Equipment.fkTeamId = Team.id AND Team.fkDepartmentId = dept_id
    GROUP BY Equipment.name;

  deptId          Department.id%TYPE;
  deptName        Department.name%TYPE;
  equipmentName   Equipment.name%TYPE;
  equipmentCount  Equipment.quantity%TYPE;
  listOfEquipment list_of_dept_equipment.list%TYPE;

  BEGIN
    DELETE FROM list_of_dept_equipment;

    IF list_dept%ISOPEN
    THEN CLOSE list_dept; END IF;

    OPEN list_dept;

    LOOP
      FETCH list_dept INTO deptId, deptName;
      EXIT WHEN list_dept%NOTFOUND;

      OPEN list_equipment(deptId);
      listOfEquipment := '';

      LOOP
        FETCH list_equipment INTO equipmentName, equipmentCount;
        EXIT WHEN list_equipment%NOTFOUND;

        listOfEquipment := listOfEquipment || '|' || equipmentName || ':' || equipmentCount;
      END LOOP;
      CLOSE list_equipment;

      INSERT INTO list_of_dept_equipment VALUES (deptId, deptName, listOfEquipment);

    END LOOP;
    CLOSE list_dept;
    COMMIT;
  END;

BEGIN
  populate_dept_equipment();
END;

CREATE OR REPLACE PROCEDURE raise_salary AS
  CURSOR list_without_equipment IS
    SELECT Employee.id
    FROM Employee
    WHERE 0 = (SELECT COUNT(*)
               FROM Equipment
               WHERE Equipment.fkOwnerId = Employee.id);
  BEGIN
    FOR employeeId IN list_without_equipment
    LOOP
      UPDATE Employee
      SET Employee.salary = Employee.salary + Employee.salary * 0.15
      WHERE Employee.id = employeeId.id;
    END LOOP;
  END;

SELECT *
FROM Employee;

BEGIN
  raise_salary();
END;

--   Lab3 Packages

CREATE OR REPLACE PACKAGE danil AS
  FUNCTION get_department_teams_count(department_name IN Department.name%TYPE)
    RETURN NUMBER;
  FUNCTION get_department_equipment_count(department_name IN Department.name%TYPE,
                                          equipment_name  IN Equipment.name%TYPE)
    RETURN NUMBER;

  PROCEDURE update_job_title(job_name IN Employee.job%TYPE, medianSalary IN Employee.salary%TYPE);
  PROCEDURE raise_salary;
END danil;

CREATE OR REPLACE PACKAGE BODY danil AS

  FUNCTION get_department_teams_count(department_name IN Department.name%TYPE)
    RETURN NUMBER
  AS teams_count NUMBER;
    BEGIN
      SELECT COUNT(*)
      INTO teams_count
      FROM Department, Team
      WHERE Department.name = department_name AND Team.fkDepartmentId = Department.id;
      RETURN (teams_count);
    END;
  --
  FUNCTION get_department_equipment_count(department_name IN Department.name%TYPE,
                                          equipment_name  IN Equipment.name%TYPE)
    RETURN NUMBER
  AS equipment_count NUMBER;
    BEGIN
      SELECT SUM(Equipment.quantity)
      INTO equipment_count
      FROM Department, Team, Equipment
      WHERE
        Department.name = department_name AND Team.fkDepartmentId = Department.id AND Equipment.fkTeamId = Team.id AND
        Equipment.name = equipment_name;
      RETURN (equipment_count);
    END;
  --
  PROCEDURE update_job_title(job_name IN Employee.job%TYPE, medianSalary IN Employee.salary%TYPE)
  AS
    BEGIN
      UPDATE Employee
      SET Employee.additionalInfo = CASE WHEN (Employee.salary < medianSalary)
        THEN concat(Employee.additionalInfo, 'Low income')
                                    ELSE concat(Employee.additionalInfo, 'High income') END
      WHERE Employee.job = job_name;
    END;
  --

  -- Exceptions

  PROCEDURE raise_salary AS
    CURSOR list_without_equipment IS
      SELECT
        Employee.id,
        Employee.salary
      FROM Employee
      WHERE 0 = (SELECT COUNT(*)
                 FROM Equipment
                 WHERE Equipment.fkOwnerId = Employee.id);

    employeeId     Employee.id%TYPE;
    employeeSalary Employee.salary%TYPE;

      ex_salary_is_to_high EXCEPTION;

    BEGIN
      OPEN list_without_equipment;
      LOOP
        FETCH list_without_equipment INTO employeeId, employeeSalary;
        EXIT WHEN list_without_equipment%NOTFOUND;

        IF employeeSalary > 150
        THEN RAISE ex_salary_is_to_high;
        END IF;

        UPDATE Employee
        SET Employee.salary = Employee.salary + Employee.salary * 0.15
        WHERE Employee.id = employeeId;
      END LOOP;
      CLOSE list_without_equipment;
      EXCEPTION
      WHEN ex_salary_is_to_high THEN
      dbms_output.put_line('Can''t raise salary because at least one employee has to big one!');
      ROLLBACK;
    END;

END danil;

BEGIN
  danil.raise_salary();
END;


-- Lab4 - Triggers

CREATE OR REPLACE TRIGGER update_team_size
AFTER INSERT OR DELETE ON Employee
FOR EACH ROW
  BEGIN
    IF INSERTING
    THEN
      UPDATE Team
      SET Team.teamSize = Team.teamSize + 1
      WHERE Team.id = :NEW.fkTeamId;
    ELSIF DELETING
      THEN
        UPDATE Team
        SET Team.teamSize = Team.teamSize - 1
        WHERE Team.id = :OLD.fkTeamId;
    END IF;
  END;


SELECT *
FROM Team;

INSERT INTO Employee VALUES (8, 'New employee', 'manager', '', 60, 1);

CREATE OR REPLACE TRIGGER low_salary_guard
BEFORE INSERT OR UPDATE OF salary ON Employee
FOR EACH ROW
  DECLARE
      too_low_salary EXCEPTION;
      too_high_salary EXCEPTION;
  BEGIN
    IF (:NEW.salary < 50)
    THEN
      RAISE too_low_salary;
      END IF;
    IF (:NEW.salary > 450)
      THEN
        RAISE too_high_salary;
    END IF;

    EXCEPTION
    WHEN too_low_salary THEN
    raise_application_error(-20324, 'Employee salary is too low');
    WHEN too_high_salary THEN
    raise_application_error(-20326, 'Employee salary is too high');
  END;

INSERT INTO Employee VALUES (9, 'New employee', 'manager', '', 15, 1);

